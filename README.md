# RunAsUnified
Run the app with one of three shell switches - PowerShell/CMD/Bash!


## Arch Linux installation guide
 
 #### Pre-built package
  * Download ![```Pre-built package which is ending with .pkg.tar.zst```](https://github.com/mrquantumoff/RunAsUnified/releases)
  * Run ```pacman -U ./RunAsUnified-<version>-x86_64.pkg.tar.zst```

  ## macOS/Other GNU/Linux distros
   * Install sudo and .NET SDK
   * Download the source code
   * ```cd``` into folder where RunAsUnified.csproj is located
   * Run ```dotnet build```
   * Then in ```bin/Debug/net6.0``` binaries will appear
   * Copy them into /bin
 ## Alternatively (Also macOS/other GNU/Linux distros)
  * Install sudo and .NET SDK
  * Download the source code
  * ```cd``` into folder where RunAsUnified.csproj is located
  * Run the build and installation script


Usage examples: 
  * ```ruas pwsh script.ps1```
  * ```ruas cmd script.cmd```
  * ```ruas bash pacman -Syu```

﻿using System.Diagnostics;
using System.IO;

namespace RunAsUnified
{
    
    public class RunAsUnified
    {

        public static void Main(string[] args)
        {
            string os = "Unknown";
            string windir = Environment.GetEnvironmentVariable("windir");
            if (!string.IsNullOrEmpty(windir) && windir.Contains(@"\") && Directory.Exists(windir)) os="windows";
            else if (File.Exists(@"/proc/sys/kernel/ostype"))
            {
                string osType = File.ReadAllText(@"/proc/sys/kernel/ostype");
                if (osType.StartsWith("Linux", StringComparison.OrdinalIgnoreCase))
                {
                    // Note: Android gets here too
                    os = "linux";
                }
            }
            else if (Directory.Exists(@"/Users")) {
                os = "mac";
            }
            string commandpath=null;
            
            string freddy = @"
            
                             ____
                             \   \
 ______            __________/\   \
|  __  |  ________ |         \/\__/
| (__) |-/       | |______    |
|______|-| ______| |  __  |   |
      ===| |00000| | |()| |   |
         | \00000| | |__| |__/
         \-------| -------
        |  _______  _________\=====
        | /       | |        |
        | |       | |        |
        | |___-_-/  -_---___-
          V V V V   V V V V V
   =====\\                  //
         \ \A            A/ /
          \ \AA        AA/ /====
           \ \_ A A A A_/ /
            \__---|   |--/";

            
            
            
            //else if (args[0]==null || args[1]==null) commandpath="placeholder";
            //else commandpath=args[1];
            
            foreach (string arg in args) {
                if (arg!="c" && arg != "cmd" && arg != "pwsh" && arg!="powershell" && arg!= "p" && arg!="b" && arg!="bash" && arg!="help" && arg!="ilovefnaf" && arg!=args[0] || arg!="--sudo"){
                    commandpath+=" ";
                    commandpath+=arg;
                }
            }

            if (args.Length == 0) Console.WriteLine("Please run the app with one of three shell switches - PowerShell/CMD/Bash! \n For example: \n ruas p script.ps1 \n ruas c script.cmd \n ruas b pacman -Syu");
            else if (args.Length == 1 && args[0] != "help" && args[0] != "h" && args[0]!="ilovefnaf") Console.WriteLine("Uh oh, it seems like you forgot one of arguments or you entered it incorrectly");
            else if (args[0] == "help") Console.WriteLine("Please run the app with one of three shell switches - PowerShell/CMD/Bash! \n For example: \n ruas p script.ps1 \n ruas c script.cmd \n ruas b pacman -Syu");
            
            else if (os=="windows"){
            if (args[0] == "c" || args[0] == "cmd") RunAs("cmd", commandpath);
            else if (args[0] == "p" || args[0] == "powershell" || args[0] == "pwsh") RunAs("pwsh", commandpath);
            else if (args[0]=="b" || args[0]=="bash") Console.WriteLine("Please run this on Linux!");
            }
            else if (os=="mac") {
            if (args[0] == "b" || args[0] == "bash") RunAs("bash", commandpath);
            else if (args[0] == "ilovefnaf") Console.WriteLine(freddy);
            else if (args[0] == "c" || args[0] == "cmd" ||args[0] == "p" || args[0] == "powershell" || args[0] == "pwsh") Console.WriteLine("Please run this on windows!");
            }
            else if (os=="linux") {
            if (args[0] == "b" || args[0] == "bash" && args[1] != "--sudo") RunAs("bash-linux", commandpath);
            else if (args[0] == "b" || args[0] == "bash" && args[1] == "--sudo") RunAs("bash", commandpath);
            else if (args[0] == "ilovefnaf") Console.WriteLine(freddy);
            else if (args[0] == "c" || args[0] == "cmd" ||args[0] == "p" || args[0] == "powershell" || args[0] == "pwsh") Console.WriteLine("Please run this on windows!");
            }
            //Console.WriteLine(args[0] + " " + args[1]);
        }
        public static void RunAs(string shell, string commandpath)
        {

            //Console.WriteLine("Running");
            string shellpath=@"C:\SPM\SPM.exe";
            string arguments = "a";
            bool verb = true;
            bool shexec = false;
             
            switch (shell)
            {
                case "cmd":
                    shellpath = @"C:\Windows\System32\cmd.exe";
                    arguments = "/c " + commandpath;
                    break;
                case "pwsh":
                    shellpath = @"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe";
                    arguments = commandpath;
                    break;
                case "bash":
                    shellpath = "/usr/bin/sudo";
                    arguments = commandpath;
                    //Console.WriteLine(commandpath);
                    //shexec = true;
                    verb = false;
                    break;
                    
                case "bash-linux":
                    shellpath = "/usr/bin/pkexec";
                    arguments = commandpath;
                    //Console.WriteLine(commandpath);
                    shexec = true;
                    verb = false;
                    break;

            }
            Process PackageStartInfo = new Process();
            PackageStartInfo.StartInfo.FileName = shellpath;
            PackageStartInfo.StartInfo.Arguments = arguments;
            PackageStartInfo.StartInfo.UseShellExecute = shexec;
            
            if (verb) PackageStartInfo.StartInfo.Verb = "runas";
            PackageStartInfo.Start();
            PackageStartInfo.WaitForExit();
            
            
            Random random = new Random();
            int res = random.Next(1, 1000);
            if (res == 102)
            {
                Console.WriteLine(@"
      ______        ______
     |  ___ \      / ___  |
      \ \  \ \    / /  / /
       \ \__\ \  / /__/ /
       | |  | |  | |  | |
       | |  | |  | |  | |
       | |__| |__| |__| |
      /  ____      ____  \
      | |(0) |    |(0) | |
      | |____| __ |____| |
      |   |---|00|----|  |
      |___|____||_____|__|
      ||  UUUUUUUUUUUU  ||
      |\__DDDDDDDDDDDD__/|
      \__________________/
");
            }

        }
    }
}